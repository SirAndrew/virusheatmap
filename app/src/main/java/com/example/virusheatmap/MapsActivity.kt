package com.example.virusheatmap


import android.content.pm.PackageManager
import android.graphics.Color
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.TileOverlayOptions
import com.google.maps.android.heatmaps.Gradient
import com.google.maps.android.heatmaps.HeatmapTileProvider
import kotlinx.android.synthetic.main.activity_maps.*
import java.lang.Double

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    val Volgograd = LatLng(48.7193900, 44.5018300)
    var CoordList: MutableList<LatLng> = mutableListOf()
    var StringList : List <String> = mutableListOf()
    var sending = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this, arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION),
                11
            )
        }
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), 123)
        }

        MyLocationListener.SetUpLocationListener(this)

        button_show.setOnClickListener{
            mMap.clear()
            var mGetList = getList()
            mGetList.execute()
        }

        send_coords.setOnClickListener{
            if(!sending) {
                SendLocation.startService(this, "")
                Toast.makeText(applicationContext, "Передача данных началась", LENGTH_SHORT ).show()
                sending = true
            }
            else{
                SendLocation.stopService(this)
                Toast.makeText(applicationContext, "Передача данных закончилась", LENGTH_SHORT ).show()
                sending = false
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Volgograd))
        val camPos = CameraPosition.Builder()
            .target(Volgograd)
            .zoom(10f)
            .bearing(0f)
            .tilt(0f)
            .build()
        val camUpd3 = CameraUpdateFactory.newCameraPosition(camPos)
        googleMap.animateCamera(camUpd3)
    }


    fun coordConvert(strings :List <String>,coords : MutableList <LatLng>): MutableList<LatLng> {
        for(i in 0 until strings.size){
            val pos = strings.get(i).indexOf(";")
            coords.add(i,LatLng(Double.valueOf(strings.get(i).substring(0, pos)), Double.valueOf(strings.get(i).substring(pos + 1))))
        }
        return coords
    }

    private fun addHeatMap() {
        val colors: IntArray = intArrayOf(
            Color.rgb(102, 225, 0), // green
            Color.rgb(255, 0, 0)    // red
        )

        val startPoints : FloatArray = floatArrayOf(
            0.2f, 1f
        )

        val gradient = Gradient(colors,startPoints)

        val mProvider = HeatmapTileProvider.Builder()
            .data(CoordList!!)
            .gradient(gradient)
            .build()

        val opt = TileOverlayOptions().tileProvider(mProvider)
        mMap.addTileOverlay(opt)

    }

    internal inner class getList : AsyncTask<Void, Void, String>() {

        override fun doInBackground(vararg params: Void?): String? {
            try {
                StringList = getCoordList(
                    getContentOfHTTPPage(
                        "https://rocky-cove-07551.herokuapp.com/api/allcoords",
                        "utf-8"
                    )
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            coordConvert(StringList,CoordList)
            Log.d(com.example.virusheatmap.TAG,CoordList.get(0).toString())
            addHeatMap()
        }
    }
}
